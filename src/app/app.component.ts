import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "AngularTestDD";
  count = 1;

  plus() {
    this.count++;
  }

  reset() {
    this.count = 0;
  }
}
