import { TestBed, async } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { By } from '@angular/platform-browser';

describe("AppComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent]
    }).compileComponents();
  }));

  it("should create the app", () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'AngularTestDD'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual("AngularTestDD");
  });

  it("should render title", () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector("h1").textContent).toContain(
      "AngularTestDD app is running!"
    );
  });

  it("should increment count while + button click", async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    expect(fixture.componentInstance.count).toBe(1);
    const button = fixture.nativeElement.querySelector("button.plus");
    button.click();
    fixture.detectChanges();
    expect(fixture.componentInstance.count).toBe(2);
  }));

  it("should RESET count while reset button click", async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    expect(fixture.componentInstance.count).toBe(1);
    const button = fixture.nativeElement.querySelector('button.reset');
    button.click();
    fixture.detectChanges();
    expect(fixture.componentInstance.count).toBe(1);
  }));
});
