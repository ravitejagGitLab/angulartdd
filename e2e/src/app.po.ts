import { browser, by, element, ElementFinder } from "protractor";

export class AppPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getTitleText(): Promise<string> {
    return element(by.css("app-root h1.title")).getText() as Promise<string>;
  }

  getCount(): Promise<string> {
    return element(by.css("app-root div.container h1")).getText() as Promise<
      string
    >;
  }

  getPlus() {
    return element(by.cssContainingText("div.container button", "+"));
  }

  getReset() {
    return element(by.cssContainingText("div.container button", "Reset"));
  }
}
