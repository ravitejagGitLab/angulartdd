import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('AngularTestDD app is running!');
  });

  it('should click 3 times and reset with matching points', () => {
    page.navigateTo();
    expect(page.getCount()).toEqual('1');

    page.getPlus().click();
    page.getPlus().click();
    page.getPlus().click();
    expect(page.getCount()).toEqual('4');

    page.getReset().click();
    expect(page.getCount()).toEqual('0');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
